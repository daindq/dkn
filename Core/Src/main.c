/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd16x2.h"
#include <stdlib.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct run_state
{
    // Variables
    uint16_t temp_ref;
    uint16_t time_ref;
    uint16_t p_ref;
    uint16_t vp_ref;
    uint8_t type;
    // Output function
    void (*callbacks_1[4])(); // For control gas valve according to control type.
    void (*callbacks_2)(); // For exhaust valve control.
    // Next states
    uint8_t next_state[8];

}RS;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//Values for main_state
#define RUN 1
#define STEP_SETTING 2
#define SETTING 3
//Value for keyPressed
#define BACK 42
#define NEXT 35
#define START 65
#define SET 66
#define CLEAR 67
//Values for setting state
#define SET_TEMP_REF 0
#define SET_TIME_REF 1
#define SET_PRESSURE_REF 2
#define SET_VALVE_POSITION 3
#define SET_CONDITION 4
	//
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
  GPIO_InitTypeDef GPIO_InitStructPrivate = {0};
  uint32_t previousMillis = 0;
  uint32_t currentMillis = 0;
  uint8_t keyPressed = 0;
  uint8_t step_number = 0;
  RS* run_state_ptr = NULL;
  uint8_t input = 0;
  uint16_t real_temp, real_press = 0;
  uint8_t sec_elapsed = 0; uint32_t current_tick = 0; uint32_t previous_tick = 0; uint8_t min_elapsed = 0;
  //  state indexes
  uint8_t main_state = STEP_SETTING;
  uint8_t step_idx = 0;
  uint8_t setting_state = SET_TEMP_REF;
  //Flags
  bool flag1 = true;bool flag2 = true; bool flag3 = true; bool flag4 = true;
  bool flag5 = false;bool reached_temp = false;bool reached_press = false;bool reached_time = false;
  // Program flags: 1 - key Press; 2 - main state switched; 4 - Display update
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */
RS* run_state_init(uint8_t num_step);
void TimeControl(void);
void TemperatureControl(void);
void PressureControl(void);
void SetValvePosition(void);
void SetStateOutput(RS* states_ptr, uint8_t state_index, uint8_t control_type);//  Set state output
uint8_t BoolToInt(bool temp, bool press, bool time);
uint16_t GetTemp(uint32_t elapsed, uint32_t Delay);
uint16_t GetPress(uint32_t elapsed, uint32_t Delay);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */
  HAL_GPIO_WritePin(GPIOB, COL1_Pin, 0);
  HAL_GPIO_WritePin(GPIOB, COL2_Pin, 0);
  HAL_GPIO_WritePin(GPIOB, COL3_Pin, 0);
  HAL_GPIO_WritePin(GPIOB, COL4_Pin, 0);
  lcd16x2_init_8bits(RS_GPIO_Port, RS_Pin, E_Pin,
				  D0_GPIO_Port, D0_Pin, D1_Pin, D2_Pin, D3_Pin,
				  D4_GPIO_Port, D4_Pin, D5_Pin, D6_Pin, D7_Pin);
  lcd16x2_printf("STARTING UP...");
  HAL_Delay(1000);
  lcd16x2_cursorShow(false);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
//	  if (flag1==true)
//	  {
//	  lcd16x2_1stLine();
//	  lcd16x2_printf("Button test:");
//	  lcd16x2_2ndLine();
//	  lcd16x2_printf("Value1 = %c", keyPressed);
//	  flag1=false;
//	  }
//	  HAL_Delay(100);
//	  lcd16x2_clear();
//	  HAL_Delay(50);
	  switch (main_state)
	          {
	          case RUN:
	        	  if(flag2){
					  flag2=false;
					  HAL_GPIO_WritePin(Time_GPIO_Port, Time_Pin, 0);
					  HAL_GPIO_WritePin(Pressure_GPIO_Port, Pressure_Pin, 0);
					  HAL_GPIO_WritePin(VALVE_GPIO_Port, VALVE_Pin, 0);
					  HAL_GPIO_WritePin(Temperature_GPIO_Port, Temperature_Pin, 0);
					  reached_temp = false; reached_press = false; reached_time = false;
					  min_elapsed = 0; sec_elapsed = 0;
				  	  lcd16x2_clear();
					  lcd16x2_1stLine();
					  lcd16x2_printf("Run step %3d/%3d", step_idx+1, step_number);

	        	  }
	        	  if(flag5){//timer flag
	        		  min_elapsed = sec_elapsed+1>60 ? min_elapsed+1 : min_elapsed;
	        		  sec_elapsed = sec_elapsed+1>60 ? 0 : sec_elapsed+1;
	        		  flag3 = true;
	        	  }
	        	  //Display
	        	  if(flag3){
					  lcd16x2_2ndLine();
					  lcd16x2_printf("T:%4d|%3dmin%2ds", real_temp, min_elapsed, sec_elapsed);
					  flag3=false;
	        	  }
//	              //Running Step by step
//	                  // 1) set output according to current state
	        	  	  run_state_ptr[step_idx].callbacks_1[run_state_ptr[step_idx].type]();
	        	  	  run_state_ptr[step_idx].callbacks_2();
//	                  // 2) read input from sensor&timer and compare to reference
//	        	  	  get sensor input
					  real_temp = GetTemp(min_elapsed, 2);
					  real_press = GetPress(min_elapsed, 2);

	        	  	  current_tick = HAL_GetTick();
					  flag5 = current_tick-previous_tick>100 ? true : false; // tick = 100ms for 10x faster simulation
					  previous_tick = current_tick-previous_tick>100 ? current_tick : previous_tick;
					  reached_time = min_elapsed==run_state_ptr[step_idx].time_ref;
	        	  	  reached_temp = real_temp>=run_state_ptr[step_idx].temp_ref;
	        	  	  reached_press = real_press>=run_state_ptr[step_idx].p_ref;
	        	  	  input = BoolToInt(reached_temp, reached_press, reached_time);
	        	  	  input = input&(0x07);
//	                  // 3) next depends on state and input
	        	  	  flag2 = run_state_ptr[step_idx].next_state[input]-step_idx==0 ? false:true;
	                  step_idx = run_state_ptr[step_idx].next_state[input];
//
	              if (step_idx >= step_number)
	              {
//	            	  Main state change condition.
	            	  free(run_state_ptr);
	                  main_state = STEP_SETTING;
	                  step_idx = 0;
	                  step_number = 0;
					  flag2=true;
	              }

	              break;

	          case SETTING:
//	        	  Display
	        	  if(flag2){
					lcd16x2_clear();
					lcd16x2_1stLine();
					lcd16x2_printf("Set step%3d/%3d ", step_idx+1, step_number);
					lcd16x2_2ndLine();
					lcd16x2_printf("Temp ref=%4d  C", run_state_ptr[step_idx].temp_ref);
					flag2 = false;
	        	  }
	        	  switch(setting_state){
					case SET_TEMP_REF:
						if(flag4){
							lcd16x2_2ndLine();
							lcd16x2_printf("Temp ref=%4d  C", run_state_ptr[step_idx].temp_ref);
							flag4=false;
						}
						if(flag1 && keyPressed <= 57 && keyPressed >=48){
							run_state_ptr[step_idx].temp_ref = ((run_state_ptr[step_idx].temp_ref)*10+keyPressed-48)>1500 ? 1500 : (run_state_ptr[step_idx].temp_ref)*10+keyPressed-48;
							flag4=true;
							flag1=false;
						}
						if (flag1 && keyPressed == CLEAR){
							run_state_ptr[step_idx].temp_ref = 0;
							flag4=true;
							flag1=false;
						}
						if(flag1 && keyPressed==NEXT){
							setting_state=SET_TIME_REF;
							flag4=true;
							flag1 = false;
						}
						if(flag1 && keyPressed==SET){
							step_idx = step_idx<step_number-1 ? step_idx+1 : step_idx;
							flag2=true;
							flag1=false;
						}
						if (flag1 && keyPressed == BACK){
						  step_idx = step_idx-1>=0 ? step_idx-1 : 0;
						  flag2 = true;
						  flag1 = false;
						}

						break;
					case SET_TIME_REF:
						if(flag4){
							lcd16x2_2ndLine();
							lcd16x2_printf("Time ref=%3dmins", run_state_ptr[step_idx].time_ref);
							flag4=false;
						}
						if(flag1 && keyPressed <= 57 && keyPressed >=48){
							run_state_ptr[step_idx].time_ref = ((run_state_ptr[step_idx].time_ref)*10+keyPressed-48)>120 ? 120 : (run_state_ptr[step_idx].time_ref)*10+keyPressed-48;
							flag4=true;
							flag1=false;
						}
						if (flag1 && keyPressed == CLEAR){
							run_state_ptr[step_idx].time_ref = 0;
							flag4=true;
							flag1=false;
						}
						if(flag1 && keyPressed==NEXT){
							setting_state=SET_PRESSURE_REF;
							flag4=true;
							flag1=false;
						}
						break;
					case SET_PRESSURE_REF:
						if(flag4){
							lcd16x2_2ndLine();
							lcd16x2_printf("Press ref%3d kPA", run_state_ptr[step_idx].p_ref);
							flag4=false;
						}
						if(flag1 && keyPressed <= 57 && keyPressed >=48){
							run_state_ptr[step_idx].p_ref = (run_state_ptr[step_idx].p_ref)*10+keyPressed-48>999 ? 999 : (run_state_ptr[step_idx].p_ref)*10+keyPressed-48;
							flag4=true;
							flag1=false;
						}
						if (flag1 && keyPressed == CLEAR){
							run_state_ptr[step_idx].p_ref = 0;
							flag4=true;
							flag1=false;
						}
						if(flag1 && keyPressed==NEXT){
							setting_state=SET_VALVE_POSITION;
							flag4=true;
							flag1=false;
						}
						break;
					case SET_VALVE_POSITION:
						if(flag4){
							lcd16x2_2ndLine();
							lcd16x2_printf("valve=%10d", run_state_ptr[step_idx].vp_ref);
							flag4=false;
						}
						if(flag1 && keyPressed <= 57 && keyPressed >=48){
							run_state_ptr[step_idx].vp_ref = ((run_state_ptr[step_idx].vp_ref)*10+keyPressed-48)>100 ? 100 : (run_state_ptr[step_idx].vp_ref)*10+keyPressed-48;
							flag4=true;
							flag1=false;
						}
						if (flag1 && keyPressed == CLEAR){
							run_state_ptr[step_idx].vp_ref = 0;
							flag4=true;
							flag1=false;
						}
						if(flag1 && keyPressed==NEXT){
							setting_state=SET_CONDITION;
							flag4=true;
							flag1=false;
						}
						break;
					case SET_CONDITION:
						if(flag4){
							lcd16x2_2ndLine();
							switch(run_state_ptr[step_idx].type){
								default:
									lcd16x2_printf("condition=%6s", "None");
								case 1:
									lcd16x2_printf("condition=%6s", "Time");
								case 2:
									lcd16x2_printf("condition=%6s", "Temp");
								case 3:
									lcd16x2_printf("condition=%6s", "Press");
							}
							flag4=false;
						}
						if(flag1 && keyPressed <= 51 && keyPressed >=49){
							run_state_ptr[step_idx].type = keyPressed-48;
							SetStateOutput(run_state_ptr, step_idx, run_state_ptr[step_idx].type);
							flag4=true;
							flag1=false;
						}
						if(flag1 && keyPressed==NEXT){
							setting_state=SET_TEMP_REF;
							SetStateOutput(run_state_ptr, step_idx, run_state_ptr[step_idx].type);
							flag4=true;
							flag1=false;
						}
						break;
	        	  }
	              if (step_idx==step_number-1 && keyPressed == START)
	              {
	                  main_state = RUN;
	                  step_idx = 0;
	                  previous_tick = HAL_GetTick();
	                  flag2 = true;
	                  flag3 = true;
	                  flag4 = true;
	              }
	              break;

	          case STEP_SETTING:
	        	  //Display
	              if(flag2){
	            	  lcd16x2_clear();
					  lcd16x2_1stLine();
					  lcd16x2_printf("Input no. steps:");
					  lcd16x2_2ndLine();
					  lcd16x2_printf("%3d", step_number);
					  flag2=false;
	              }
	              if(flag1 && keyPressed <= 57 && keyPressed >=48){
		              step_number = step_number*10+keyPressed-48>100 ? 100 : step_number*10+keyPressed-48;
					  run_state_ptr = step_number>0 ? run_state_init(step_number) : NULL;
					  flag2=true;
					  flag1=false;
	              }
	              if (flag1 && keyPressed == CLEAR){
	            	  step_number = 0;
	            	  flag2=true;
					  flag1=false;
	              }
	              if (keyPressed == SET && run_state_ptr != NULL && step_number>1)
	              {
	                  main_state = SETTING;
	                  step_idx=0;
	                  flag1=false;
	                  flag2 = true;
	                  flag3 = true;
	              }
	              break;

	          }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, VALVE_Pin|Time_Pin|Pressure_Pin|Temperature_Pin
                          |RS_Pin|E_Pin|D0_Pin|D1_Pin
                          |D2_Pin|D3_Pin|D4_Pin|D5_Pin
                          |D6_Pin|D7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, COL1_Pin|COL2_Pin|COL3_Pin|COL4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : VALVE_Pin Time_Pin Pressure_Pin Temperature_Pin
                           RS_Pin E_Pin D0_Pin D1_Pin
                           D2_Pin D3_Pin D4_Pin D5_Pin
                           D6_Pin D7_Pin */
  GPIO_InitStruct.Pin = VALVE_Pin|Time_Pin|Pressure_Pin|Temperature_Pin
                          |RS_Pin|E_Pin|D0_Pin|D1_Pin
                          |D2_Pin|D3_Pin|D4_Pin|D5_Pin
                          |D6_Pin|D7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : COL1_Pin COL2_Pin COL3_Pin COL4_Pin */
  GPIO_InitStruct.Pin = COL1_Pin|COL2_Pin|COL3_Pin|COL4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : ROW1_Pin ROW2_Pin ROW3_Pin ROW4_Pin */
  GPIO_InitStruct.Pin = ROW1_Pin|ROW2_Pin|ROW3_Pin|ROW4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
/**
  * @brief EXTI callback
  * @param GPIO_Pin
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	flag1 = true;
	currentMillis = HAL_GetTick();
	if (currentMillis - previousMillis > 10) {
		/*Configure GPIO pins : ROW1 ROW2 ROW3 ROW4 to GPIO_INPUT*/
		GPIO_InitStructPrivate.Pin = ROW1_Pin|ROW2_Pin|ROW3_Pin|ROW4_Pin;
		GPIO_InitStructPrivate.Mode = GPIO_MODE_INPUT;
		GPIO_InitStructPrivate.Pull = GPIO_PULLUP;
		GPIO_InitStructPrivate.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructPrivate);

		HAL_GPIO_WritePin(GPIOB, COL1_Pin, 1);
		HAL_GPIO_WritePin(GPIOB, COL2_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL3_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL4_Pin, 0);
		if(GPIO_Pin == ROW1_Pin && HAL_GPIO_ReadPin(GPIOB, ROW1_Pin))
		{
		  keyPressed = 49; //ASCII value of 1
		}
		else if(GPIO_Pin == ROW2_Pin && HAL_GPIO_ReadPin(GPIOB, ROW2_Pin))
		{
		  keyPressed = 52; //ASCII value of 4
		}
		else if(GPIO_Pin == ROW3_Pin && HAL_GPIO_ReadPin(GPIOB, ROW3_Pin))
		{
		  keyPressed = 55; //ASCII value of 7
		}
		else if(GPIO_Pin == ROW4_Pin && HAL_GPIO_ReadPin(GPIOB, ROW4_Pin))
		{
		  keyPressed = 42; //ASCII value of *
		}

		HAL_GPIO_WritePin(GPIOB, COL1_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL2_Pin, 1);
		HAL_GPIO_WritePin(GPIOB, COL3_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL4_Pin, 0);
		if(GPIO_Pin == ROW1_Pin && HAL_GPIO_ReadPin(GPIOB, ROW1_Pin))
		{
		  keyPressed = 50; //ASCII value of 2
		}
		else if(GPIO_Pin == ROW2_Pin && HAL_GPIO_ReadPin(GPIOB, ROW2_Pin))
		{
		  keyPressed = 53; //ASCII value of 5
		}
		else if(GPIO_Pin == ROW3_Pin && HAL_GPIO_ReadPin(GPIOB, ROW3_Pin))
		{
		  keyPressed = 56; //ASCII value of 8
		}
		else if(GPIO_Pin == ROW4_Pin && HAL_GPIO_ReadPin(GPIOB, ROW4_Pin))
		{
		  keyPressed = 48; //ASCII value of 0
		}

		HAL_GPIO_WritePin(GPIOB, COL1_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL2_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL3_Pin, 1);
		HAL_GPIO_WritePin(GPIOB, COL4_Pin, 0);
		if(GPIO_Pin == ROW1_Pin && HAL_GPIO_ReadPin(GPIOB, ROW1_Pin))
		{
		  keyPressed = 51; //ASCII value of 3
		}
		else if(GPIO_Pin == ROW2_Pin && HAL_GPIO_ReadPin(GPIOB, ROW2_Pin))
		{
		  keyPressed = 54; //ASCII value of 6
		}
		else if(GPIO_Pin == ROW3_Pin && HAL_GPIO_ReadPin(GPIOB, ROW3_Pin))
		{
		  keyPressed = 57; //ASCII value of 9
		}
		else if(GPIO_Pin == ROW4_Pin && HAL_GPIO_ReadPin(GPIOB, ROW4_Pin))
		{
		  keyPressed = 35; //ASCII value of #
		}


		HAL_GPIO_WritePin(GPIOB, COL1_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL2_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL3_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL4_Pin, 1);
		if(GPIO_Pin == ROW1_Pin && HAL_GPIO_ReadPin(GPIOB, ROW1_Pin))
		{
		  keyPressed = 65; //ASCII value of A
		}
		else if(GPIO_Pin == ROW2_Pin && HAL_GPIO_ReadPin(GPIOB, ROW2_Pin))
		{
		  keyPressed = 66; //ASCII value of B
		}
		else if(GPIO_Pin == ROW3_Pin && HAL_GPIO_ReadPin(GPIOB, ROW3_Pin))
		{
		  keyPressed = 67; //ASCII value of C
		}
		else if(GPIO_Pin == ROW4_Pin && HAL_GPIO_ReadPin(GPIOB, ROW4_Pin))
		{
		  keyPressed = 68; //ASCII value of D
		}


		HAL_GPIO_WritePin(GPIOB, COL1_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL2_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL3_Pin, 0);
		HAL_GPIO_WritePin(GPIOB, COL4_Pin, 0);
		/*Configure GPIO pins : PB6 PB7 PB8 PB9 back to EXTI*/
		GPIO_InitStructPrivate.Mode = GPIO_MODE_IT_FALLING;
		GPIO_InitStructPrivate.Pull = GPIO_PULLUP;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructPrivate);
		previousMillis = currentMillis;
	}
}
/**
  * @brief Dynamic memory allocation for run states
  * @param num_step - number of step
  * @retval pointer - RS*
  */
RS* run_state_init(uint8_t num_step){
    RS* ptr = (RS*)calloc(num_step, sizeof(RS));
    for(uint8_t i = 0; i < num_step; i++){
    	ptr[i].callbacks_1[0] = NULL;
    	ptr[i].callbacks_1[1] = TimeControl;
    	ptr[i].callbacks_1[2] = TemperatureControl;
    	ptr[i].callbacks_1[3] = PressureControl;
    	ptr[i].callbacks_2 = SetValvePosition;
    	for(uint8_t j = 0; j < 8; j++){
    		ptr[i].next_state[j] = i;
    	}
    }
    return ptr;
}
void TimeControl(void){
	HAL_GPIO_WritePin(Time_GPIO_Port, Time_Pin, 1);
	return;
}
void PressureControl(void){
	HAL_GPIO_WritePin(Pressure_GPIO_Port, Pressure_Pin, 1);
	return;
}
void TemperatureControl(void){
	HAL_GPIO_WritePin(Temperature_GPIO_Port, Temperature_Pin, 1);
	return;
}
void SetValvePosition(void){
	HAL_GPIO_WritePin(VALVE_GPIO_Port, VALVE_Pin, 1);
	return;
}
void SetStateOutput(RS* states_ptr, uint8_t state_index, uint8_t control_type){
	switch(control_type){
	case 1:
		states_ptr[state_index].next_state[0] = state_index;
		states_ptr[state_index].next_state[1] = state_index+1;
		states_ptr[state_index].next_state[2] = state_index;
		states_ptr[state_index].next_state[3] = state_index+1;
		states_ptr[state_index].next_state[4] = state_index;
		states_ptr[state_index].next_state[5] = state_index+1;
		states_ptr[state_index].next_state[6] = state_index;
		states_ptr[state_index].next_state[7] = state_index+1;
		break;

	case 2:
		states_ptr[state_index].next_state[0] = state_index;
		states_ptr[state_index].next_state[1] = state_index;
		states_ptr[state_index].next_state[2] = state_index+1;
		states_ptr[state_index].next_state[3] = state_index+1;
		states_ptr[state_index].next_state[4] = state_index;
		states_ptr[state_index].next_state[5] = state_index;
		states_ptr[state_index].next_state[6] = state_index+1;
		states_ptr[state_index].next_state[7] = state_index+1;
		break;
	case 3:
		states_ptr[state_index].next_state[0] = state_index;
		states_ptr[state_index].next_state[1] = state_index;
		states_ptr[state_index].next_state[2] = state_index;
		states_ptr[state_index].next_state[3] = state_index;
		states_ptr[state_index].next_state[4] = state_index+1;
		states_ptr[state_index].next_state[5] = state_index+1;
		states_ptr[state_index].next_state[6] = state_index+1;
		states_ptr[state_index].next_state[7] = state_index+1;
		break;
	default:
		states_ptr[state_index].next_state[0] = state_index;
		states_ptr[state_index].next_state[1] = state_index;
		states_ptr[state_index].next_state[2] = state_index;
		states_ptr[state_index].next_state[3] = state_index;
		states_ptr[state_index].next_state[4] = state_index;
		states_ptr[state_index].next_state[5] = state_index;
		states_ptr[state_index].next_state[6] = state_index;
		states_ptr[state_index].next_state[7] = state_index;
		break;
	}
	return;

}
uint8_t BoolToInt(bool temp, bool press, bool time){
	uint8_t input = 0;
	input = time? input+1 : input;
	input = press? input+2 : input;
	input = temp? input+4 : input;
	return input;
}
uint16_t GetTemp(uint32_t elapsed, uint32_t Delay){
	uint16_t real_temp = elapsed>=Delay ? 1000 : 500;
	return real_temp;
}
uint16_t GetPress(uint32_t elapsed, uint32_t Delay){
	uint16_t real_press = elapsed>=Delay ? 150 : 50;
	return real_press;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define VALVE_Pin GPIO_PIN_0
#define VALVE_GPIO_Port GPIOA
#define Time_Pin GPIO_PIN_1
#define Time_GPIO_Port GPIOA
#define Pressure_Pin GPIO_PIN_2
#define Pressure_GPIO_Port GPIOA
#define Temperature_Pin GPIO_PIN_3
#define Temperature_GPIO_Port GPIOA
#define RS_Pin GPIO_PIN_6
#define RS_GPIO_Port GPIOA
#define E_Pin GPIO_PIN_7
#define E_GPIO_Port GPIOA
#define COL1_Pin GPIO_PIN_0
#define COL1_GPIO_Port GPIOB
#define COL2_Pin GPIO_PIN_1
#define COL2_GPIO_Port GPIOB
#define COL3_Pin GPIO_PIN_2
#define COL3_GPIO_Port GPIOB
#define D0_Pin GPIO_PIN_8
#define D0_GPIO_Port GPIOA
#define D1_Pin GPIO_PIN_9
#define D1_GPIO_Port GPIOA
#define D2_Pin GPIO_PIN_10
#define D2_GPIO_Port GPIOA
#define D3_Pin GPIO_PIN_11
#define D3_GPIO_Port GPIOA
#define D4_Pin GPIO_PIN_12
#define D4_GPIO_Port GPIOA
#define D5_Pin GPIO_PIN_13
#define D5_GPIO_Port GPIOA
#define D6_Pin GPIO_PIN_14
#define D6_GPIO_Port GPIOA
#define D7_Pin GPIO_PIN_15
#define D7_GPIO_Port GPIOA
#define COL4_Pin GPIO_PIN_3
#define COL4_GPIO_Port GPIOB
#define ROW1_Pin GPIO_PIN_4
#define ROW1_GPIO_Port GPIOB
#define ROW1_EXTI_IRQn EXTI4_IRQn
#define ROW2_Pin GPIO_PIN_5
#define ROW2_GPIO_Port GPIOB
#define ROW2_EXTI_IRQn EXTI9_5_IRQn
#define ROW3_Pin GPIO_PIN_6
#define ROW3_GPIO_Port GPIOB
#define ROW3_EXTI_IRQn EXTI9_5_IRQn
#define ROW4_Pin GPIO_PIN_7
#define ROW4_GPIO_Port GPIOB
#define ROW4_EXTI_IRQn EXTI9_5_IRQn

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
